# Tag Cloud Page
![Tag Cloud Page](./screenshot.jpg) 

## Description
The Tag Cloud Page module creates a page in your Gallery that contains a list of all Tags used in the gallery. This list is similar to the popular tags sidebar, with the exception that it contains every tag, not just the top 30. This module also has support for the Tag Cloud module, which allows for a flash animated version of the tag cloud.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "tag_cloud_page" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 1.0.2:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 08 August 2011. 
>
> Download: [Version 1.0.2](/uploads/f2c6466b900c824571d9fce331920fc7/tag_cloud_page102.zip)

**Version 1.0.1:**
> - Display a warning message when activating this module if the Tags module is not already active.
> - Display a warning message when deactivating the Tags module, if this module is still active.
> - Released 15 May 2011.
>
> Download: [Version 1.0.1](/uploads/0a40a0ad062720ad5fea911178dd145a/tag_cloud_page101.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 15 March 2011.
>
> Download: [Version 1.0.0](/uploads/11a8a63895b7b5272fc99d7b55692993/tag_cloud_page100.zip)
